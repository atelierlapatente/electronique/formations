# Formations en électronique, informatique et fabrication numérique

Document collaboratif, chacune et chacun est invité à le modifier et faire des suggestions!

## Formations en électronique

### Soudure
**Buts:** Savoir souder et déssouder des composants électroniques sur un circuit. Savoir épisser (joindre deux fils électriques).

**Description:**

**Pré-requis:** Aucun
**Durée:** 2 heures
**Formateur(s):** ??

### Composants électroniques
**Buts:** Savoir reconnaître les différents composants d'un circuit électronique et comprendre leur utilité.

**Description:** En suivant cette formation, vous apprendrez à connaître les composants de base d'un circuit électronique: 
résistances, capaciteurs, inductances, diodes, transistors, etc. Vous apprendrez également leur utilité, comment développer un circuit simple
et où trouver de l'information pour continuer à en savoir plus!

**Pré-requis:** Formation à la soudure recommandée mais pas indispensable.
**Durée:** 3 x 2 heures
**Formateur(s):** Lionel

### Arduino
**Buts:** Savoir programmer un Arduino et connaître ses champs d'application.

**Description:**

**Pré-requis:** Aucun. Une connaissance de base en programmation et en composants électroniques est utile pour développer des applications plus complexes.
**Durée:** 3 heures
**Formateur(s):** Simon?

### Raspberry Pi
**Buts:** Savoir configurer et programmer un Raspberry Pi et connaître ses champs d'application.

**Description:** Le Raspberry Pi est un nano-ordinateur compact et bon marché qui fonctionne sous Linux. Grâce à sa plateforme modulaire, on peut aisément
lui ajouter des modules permettant d'interagir avec d'autres composants et le programmer pour une vaste quantité d'applications. Il peut servir par exemple de
station météo, de contrôleur pour une installation hydroponique ou pour un écran intelligent, ou pour piloter un robot. Dans cette formation, vous apprendrez à configurer
un module Raspberry Pi, le connecter avec des composants électroniques et écrire un programme simple pour en faire une station météo!

**Contenu:**
* Introduction aux différents modèles de Raspberry Pi
* Configuration: flashage de carte SD, installation d'un système d'exploitation, connexion à un réseau
* Prise en main
* Communiquer avec des capteurs et actuateurs via les ports GPIO
* Développement d'un petit programme pour mesurer la température et afficher les résultats sur un écran LCD
* Ressources pour aller plus loin
* Si le temps le permet, introduction à la configuration "headless" (sans écran ni clavier)

**Pré-requis:** Introduction à Linux (ou connaissances préalables de Linux). Quelques bases en programmation et en composants électroniques est utile pour développer des applications plus complexes.
**Durée:** 4 heures
**Formateur(s):** Thomas

## Formations en informatique

**Matériel:** Pour les formations en informatique, les participants sont invités à apporter leur propre ordinateur portable s'ils en ont un. Tous les logiciels utilisés dans
ces formations sont libres et gratuits, et le formateur sera à disposition pour aider les participants à les installer sur leur ordinateur si nécessaire.
Ceci afin de leur permettre de continuer à utiliser les outils et connaissances apprises une fois la formation terminée. Des ordinateurs sont également à disposition à la
Patente pour les participants ne disposant pas d'un ordinateur.

### Introduction à la ligne de commande (shell)
**Buts:** Savoir contrôler un ordinateur en utilisant la ligne de commande.

**Description:** Les systèmes d'exploitation modernes permettent à tout un chacun d'utiliser un ordinateur de façon intuitive grâce à l'usage d'une souris et d'une
interface graphique (menus, fenêtres). Cependant, il reste possible d'utiliser la ligne de commande à la place: l'usager écrit des ordres au clavier
que l'ordinateur effectue avant d'en afficher le résultat à l'écran. Cette approche reste très utile dans de nombreux champs d'application, tel que le contrôle à distance d'un serveur (connexion SSH), ou pour automatiser des opérations qui seraient trop fastidieuses de faire à la main (par exemple renommer un grand nombre de fichiers).
Cette formation a pour but de démystifier la ligne de commande: loin d'être le seul apanage des "hackers", c'est un outil très puissant pour en faire plus avec votre ordinateur.
C'est également une excellente façon de débuter en programmation, car un programme informatique est la combinaison de plusieurs instructions écrites. Cursus adapté de [Software Carpentry](https://swcarpentry.github.io/shell-novice/).

Les participants sont encouragés à apporter leur propre ordinateur portable (mac, windows ou linux) s’ils en ont un, sinon des ordinateurs seront mis à disposition des participants (sous réserve de disponibilité en cas de forte demande).

Les cours d’informatique proposés par la Patente s’inscrivent dans une logique de progression (voir schéma ci-dessous). Une bonne connaissance de la ligne de commande est une base requise pour pouvoir ensuite suivre les cours d’initiation à la programmation et Linux.

**Contenu:**
* Introduction: qu'est-ce que la ligne de commande et pourquoi l'utiliser?
* Parcourir l'arborescence des fichiers sur un ordinateur
* Opérations sur des fichiers (créer, déplacer, renommer, effacer)
* Combiner plusieurs opérations (pipes)
* Effectuer des opérations en boucle
* Écrire un script et le réutiliser
* Ressources pour aller plus loin

**Pré-requis:** Aucun
**Durée:** 3 heures
**Formateur(s):** Thomas

### Bases de la programmation (Python)
**Buts:** Connaître les bases de la programmation.

**Description:** Un programme informatique est une série d'instructions et opérations destinés à être effectuées par un ordinateur. Dans cette formation, vous apprendrez
les concepts de base de la programmation: opérations logiques, boucles, variables, opérations mathématiques, lecture et écriture de fichiers, etc. Ces concepts sont
universels et se retrouvent dans les nombreux languages de programmation qui existent (Java, JavaScript, C, C++, Basic, PHP, Ruby, FORTRAN, etc.). Nous utiliserons ici le language
Python, mais les concepts appris durant la formation sont également applicables à d'autres languages. Les participants sont invités à partager leurs propres projets,
nous prendrons le temps durant la formation pour en discuter et évoquer les options possibles pour les réaliser. Au terme de la formation, les participants seront en mesure
d'écrire un programme simple et disposeront de ressources pour aller plus loin.

**Contenu:**
* Concepts fondamentaux: variables et fonctions
* Test logiques
* Boucles (opérations répétées)
* Création de fonctions
* Lecture et écriture de fichiers
* Utilisation de modules et bibliothèques de fonctions
* Classes et objets
* Gestion des erreurs et débogage
* Ressources pour aller plus loin

**Pré-requis:** Introduction à la ligne de commande
**Durée:** 4 heures
**Formateur(s):** Thomas

### Introduction au code collaboratif et au contrôle de versions (git, GitLab, GitHub)
**Buts:** Apprendre à utiliser git pour garder la trace de son travail et collaborer sur un projet hébergé sur GitLab ou GitHub.

**Description:** Avez-vous déjà travaillé sur un projet où vous ne saviez plus quelle était la dernière version d'un document, qui l'avait modifié en dernier, et surtout
où avaient passé les 15 paragraphes que vous aviez écrit avant que quelqu'un d'autre ne reprenne le document? Ou travaillé toute une soirée à modifier un programme pour au
final vous rendre compte que la première version fonctionnait beaucoup mieux, si seulement vous pouviez remonter le temps pour repartir de là... Le contrôle de version est
là pour éviter ce type d'ennuis! git est un logiciel qui permet de garder la trace de tous les changements effectués sur des fichiers, ce qui permet de revenir en arrière si
nécessaire, ou travailler sur plusieurs versions d'un même projet en parallèle. C'est également la technologie qui est au coeur des plateformes [GitLab](https://gitlab.com/) 
et [GitHub](https://github.com/) qui sont très
largement utilsées pour collaborer sur des projets de logiciels libres. La Patente utilise GitLab et le document que vous êtes en train de lire a été écrit collaborativement 
sur cette plateforme! Cette formation vous permettra d'apprendre les concepts de base du contrôle de versions, l'usage du logiciel git et comment collaborer sur un projet 
hébergé sur GitLab ou GitHub. Cursus adapté de [Software Carpentry](https://swcarpentry.github.io/git-novice/).

**Contenu:**
* Introduction: qu'est-ce que le contrôle de version et pourquoi l'utiliser?
* Configuration et mise en place de git
* Création d'un répertoire sous contrôle de version
* Soumettre des changements et revenir en arrière
* Partager son code sur GitLab
* Collaborer sur un projet hébergé sur GitLab

**Pré-requis:** Introduction à la ligne de commande.
**Durée:** 3 heures
**Formateur(s):** Thomas

### Modélisation 2D et 3D avec OpenSCAD
**Buts:** Savoir utiliser un langage paramétrique pour concevoir des objets en vue de la fabrication numérique (impression 3D, découpe laser, etc.)

**Description:** Pour pouvoir fabriquer un objet, il est nécessaire au préalable de disposer d'un modèle de cet objet qu'un ordinateur peut comprendre et transformer en
série d'opérations destinées à une imprimante 3D ou une découpeuse laser ou CNC. Il existe pour cela des logiciels tels que SketchUp qui permettent de modéliser graphiquement
des objets, cependant il est parfois difficile de contrôler précisément la taille des objets ainsi définis, ce qui peut être problématique lorsqu'on cherche à créer une pièce
devant s'emboîter parfaitement dans une autre, par exemple. D'autres logiciels comme AutoCAD sont plus puissants, mais aussi complexes et onéreux. Le logiciel libre et gratuit
[OpenSCAD](https://openscad.org/about.html) permet de modéliser des objets complexes à partir de formes géométriques simples définies dans un langage de programmation propre.
Cela permet notamment le développement de modèles *paramétriques*, c'est à dire dont les dimensions peuvent être contrôlées par des variables. Par exemple, il est possible
de varier le diamètre et le pas de vis d'un connecteur pour tuyau d'aspirateur et ainsi l'adapter à différents modèles.

**Pré-requis:** Introduction à la ligne de commande et bases de la programmation. Des connaissances préalables de modélisation avec SketchUp ou équivalent peuvent être utiles à
appréhender les concepts de la conception assistée par ordinateur (CAO) mais ne sont pas nécessaires.
**Durée:** 4 heures
**Formateur(s):** Thomas

### Introduction à Linux
**Buts:** 

**Description:** À venir

**Pré-requis:** Introduction à la ligne de commande
**Durée:** 3 heures
**Formateur(s):** Thomas, François?

## Formations en fabrication numérique

### Conception Assistée par Ordinateur (CAO) et préparation de fichiers pour fabrication numérique
**Buts:** Savoir modéliser des objets à fabriquer ou modifier des modèles existants.

**Description:** À venir

**Pré-requis:** Aucun
**Durée:**
**Formateur(s):** François, Laurent?

### Impression 3D
**Buts:** Savoir utiliser les imprimantes 3D

**Description:** À venir

**Pré-requis:** Aucun pour imprimer des modèles téléchargés sur Internet. Les formations Modélisation avec SketchUp
ou openSCAD seront utiles pour modifier les modèles téléchargés ou en créer de toutes pièces.
**Durée:**
**Formateur(s):** Clarence

### Découpe laser
**Buts:** Savoir utiliser le graveur laser

**Description:** Ce cours vous permet de comprendre le mécanisme derrière la machine en passant par la création de votre design dans un logiciel de dessin libre (inkscape, krita, gimp, etc) jusqu'à l'impression de votre modèle. Tout les logiciels sont libres de droit et pourront être installé sur les ordinateurs des participants bien qu'ils soient aussi disponible sur les stations de travail de l'atelier la Patente.

**Pré-requis:** ---
**Durée:**
**Formateur(s):** Clarence

### Introduction à G-Code
**Buts:** Apprendre les bases du G-Code pour l'usinage numérique

**Description:** Les logiciels de conception assistée par ordinateur (CAO) permettent aujourd'hui de modéliser des objets complexes et de les transformer en série d'instructions
pour les fabriquer au moyen d'un imprimante 3D, une machine à découper (laser, plasma, jet d'eau) ou usinage à commande numérique (CNC). Dans ce dernier cas, la fabrication
nécessite le passage d'un ou plusieurs outils de découpe afin de retirer de la matière. Lors de la conception d'une pièce à fabriquer par CNC, il est donc important de
prévoir assez d'espace pour le passage de ces outils. Le langage G-code est utilisé pour définir les opérations de découpe à envoyer à une machine CNC. Une certaine connaissance
du G-code est donc utile pour comprendre quelles instructions de découpe sont générées par un logiciel CAO, vérifier leur pertinence, planifier les outils à utiliser et s'assurer
que la pièce sera fabriquée correctement et de façon sécuritaire.

**Pré-requis:** ---
**Durée:**
**Formateur(s):** François? Jonathan Blouin?

### Usinage numérique - CNC
**Buts:** Permettre aux participants d’être en mesure de comprendre les rudiments d’une machine à commande numérique et de l’opérer en mode manuelle ou avec une programmation en code-g. 

**Description:** Les machines CNC sont des machines de la même famille que les graveurs laser ou les imprimantes 3D. Leur rôle est de vous assister dans la fabrication d’objet qui serait autrement trop complexe à fabriquer. Cette formation d’introduction à l’utilisation d’une CNC s’inscrit dans une suite de formation ex.: Compréhension du code-G, Fabrication assistée par ordinateur.

**Pré-requis:** Optionel: Formation G-Code, Introduction à la FAO, Introduction à la CAO
**Durée:** 3 heures
**Formateur(s):** François

## Plan de formation suggéré
Les formations proposées sont conçues pour être combinées et fournir les bases nécessaires aux sujets plus complexes.
Aucun prérequis formel n'est demandé, le plan de formation ci-dessous est une suggestion pour combiner plusieurs
formations.

![Plan de formation](./plan-formation-electro.svg)
